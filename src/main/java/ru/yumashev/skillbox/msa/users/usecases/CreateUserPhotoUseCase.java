package ru.yumashev.skillbox.msa.users.usecases;

import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface CreateUserPhotoUseCase {
    void create(UUID userId, MultipartFile file);
}
