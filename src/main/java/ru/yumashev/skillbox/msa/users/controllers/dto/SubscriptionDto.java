package ru.yumashev.skillbox.msa.users.controllers.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class SubscriptionDto {
    private UUID id;
    private String nickname;
}
