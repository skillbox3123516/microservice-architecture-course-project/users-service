package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yumashev.skillbox.msa.users.entities.Subscription;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.SubscriptionRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.SubscriptionsUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SubscriptionsUseCaseImpl implements SubscriptionsUseCase {

    private final UserRepository userRepository;
    private final SubscriptionRepository subscriptionRepository;

    @Override
    @Transactional
    public void subscribe(UUID subscriberId, UUID subscriptionId) {
        final User subscriber = userRepository.findById(subscriberId)
            .orElseThrow(() -> new UserNotFoundException(subscriberId));
        final User subscription = userRepository.findById(subscriptionId)
            .orElseThrow(() -> new UserNotFoundException(subscriptionId));
        if (subscriptionRepository.findBySubscriberAndSubscription(subscriber, subscription).isEmpty()) {
            final Subscription subscriptionItem = new Subscription();
            subscriptionItem.setSubscriber(subscriber);
            subscriptionItem.setSubscription(subscription);
            subscriptionRepository.save(subscriptionItem);
        }
    }

    @Override
    @Transactional
    public void unsubscribe(UUID subscriberId, UUID subscriptionId) {
        final User subscriber = userRepository.findById(subscriberId)
            .orElseThrow(() -> new UserNotFoundException(subscriberId));
        final User subscription = userRepository.findById(subscriptionId)
            .orElseThrow(() -> new UserNotFoundException(subscriptionId));
        subscriptionRepository.findBySubscriberAndSubscription(subscriber, subscription)
            .ifPresent(subscriptionRepository::delete);
    }


}
