package ru.yumashev.skillbox.msa.users.controllers.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import ru.yumashev.skillbox.msa.users.entities.Gender;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Builder
@Data
public class UpdateUserDto {
    private String firstName;
    private String lastName;
    private String middleName;
    private Gender gender;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
    private String iconUrl;
    private String about;
    @NotNull
    private String nickname;
    @NotNull
    private String email;
    @NotNull
    private String phone;
    private CityDto city;
}
