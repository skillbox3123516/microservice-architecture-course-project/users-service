package ru.yumashev.skillbox.msa.users.controllers.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import ru.yumashev.skillbox.msa.users.entities.Gender;

import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
public class UserDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private String middleName;
    private Gender gender;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
    private String iconUrl;
    private String about;
    private String nickname;
    private String email;
    private String phone;
    private CityDto city;
}
