package ru.yumashev.skillbox.msa.users.usecases;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.yumashev.skillbox.msa.users.controllers.dto.UserDto;

import java.util.UUID;

public interface GetUsersUseCase {
    UserDto getById(UUID id);

    Page<UserDto> getAllUsers(Pageable pageable);
}
