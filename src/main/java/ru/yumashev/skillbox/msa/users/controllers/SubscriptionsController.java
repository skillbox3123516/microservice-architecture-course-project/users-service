package ru.yumashev.skillbox.msa.users.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriberDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriptionDto;

import java.util.List;
import java.util.UUID;

@RequestMapping("/subscriptions")
public interface SubscriptionsController {

    @PostMapping("/{subscriberId}/to/{subscriptionId}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Подписка")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())
        })
    void createSubscription(@PathVariable UUID subscriberId, @PathVariable UUID subscriptionId);

    @DeleteMapping("/{subscriberId}/to/{subscriptionId}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Отписка")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void deleteSubscription(@PathVariable UUID subscriberId, @PathVariable UUID subscriptionId);

    @GetMapping("/subscriptions/{subscriberId}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Подписки")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    List<SubscriptionDto> getSubscriptions(@PathVariable UUID subscriberId);

    @GetMapping("/subscribers/{subscriptionId}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Подписчики")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    List<SubscriberDto> getSubscribers(@PathVariable UUID subscriptionId);

}
