package ru.yumashev.skillbox.msa.users.controllers;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UpdateUserDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UserDto;

import java.util.UUID;

@RequestMapping("/users")
public interface UsersController {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Создание пользователя")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "201", description = "OK",
                content = @Content(schema = @Schema(implementation = CreateUserDto.class))),
            @ApiResponse(responseCode = "400", description = "Неправильный формат входных параметров", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    UUID createUser(@RequestBody CreateUserDto createUserDto);

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Обновление пользователя")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "404", description = "Пользователь не найден", content = @Content()),
        @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void updateUser(@PathVariable UUID id, @RequestBody UpdateUserDto updateUserDto);

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Удаление пользователя")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "404", description = "Пользователь не найден", content = @Content()),
        @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void deleteUser(@PathVariable UUID id);

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Получение пользователя")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "404", description = "Пользователь не найден", content = @Content()),
        @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    UserDto getUser(@PathVariable UUID id);

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Получение пользователей")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "OK"),
        @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    Page<UserDto> getAllUsers(@RequestParam(required = false) Pageable pageable);
}
