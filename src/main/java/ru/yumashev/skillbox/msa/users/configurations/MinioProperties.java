package ru.yumashev.skillbox.msa.users.configurations;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "minio")
public class MinioProperties {
    private String host;
    private String accessKey;
    private String secretKey;
    private String api;
}
