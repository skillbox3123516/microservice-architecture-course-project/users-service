package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriberDto;
import ru.yumashev.skillbox.msa.users.repositories.SubscriptionRepository;
import ru.yumashev.skillbox.msa.users.usecases.GetSubscribersUseCase;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GetSubscribersUseCaseImpl implements GetSubscribersUseCase {

    private final SubscriptionRepository subscriptionRepository;

    @Override
    @Transactional
    public List<SubscriberDto> getAll(UUID subscriptionId) {
        return subscriptionRepository.findBySubscriptionId(subscriptionId)
            .stream()
            .map(subscription -> SubscriberDto.builder()
                .id(subscription.getSubscriber().getId())
                .nickname(subscription.getSubscriber().getNickname())
                .build())
            .collect(Collectors.toList());
    }
}
