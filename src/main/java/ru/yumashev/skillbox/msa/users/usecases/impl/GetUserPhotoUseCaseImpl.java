package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserPhotoRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.repositories.impl.UserPhotoRepositoryException;
import ru.yumashev.skillbox.msa.users.usecases.GetUserPhotoUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserPhotoNotFoundException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GetUserPhotoUseCaseImpl implements GetUserPhotoUseCase {

    private final UserRepository userRepository;
    private final UserPhotoRepository userPhotoRepository;

    @Transactional(readOnly = true)
    @Override
    public MultipartFile get(UUID userId) {
        final User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        if (user.getIconUrl() != null) {
            try {
                return userPhotoRepository.findByObjectName(user.getId().toString());
            } catch (UserPhotoRepositoryException e) {
                throw new UserPhotoNotFoundException(userId);
            }
        } else {
            throw new UserPhotoNotFoundException(userId);
        }
    }
}
