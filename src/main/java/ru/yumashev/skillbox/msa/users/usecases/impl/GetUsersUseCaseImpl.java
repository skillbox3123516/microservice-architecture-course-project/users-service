package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yumashev.skillbox.msa.users.controllers.dto.CityDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UserDto;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.GetUsersUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.Optional;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class GetUsersUseCaseImpl implements GetUsersUseCase {

    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDto getById(UUID id) {
        return userRepository.findById(id)
            .map(this::map)
            .orElseThrow(() -> new UserNotFoundException(id));
    }

    private UserDto map(User user) {
        return UserDto.builder()
            .id(user.getId())
            .nickname(user.getNickname())
            .email(user.getEmail())
            .phone(user.getPhone())
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .middleName(user.getMiddleName())
            .about(user.getAbout())
            .gender(user.getGender())
            .birthDate(user.getBirthDate())
            .iconUrl(user.getIconUrl())
            .city(Optional.ofNullable(user.getCity())
                .map(city -> CityDto.builder()
                    .id(city.getId())
                    .name(city.getName())
                    .build())
                .orElse(null))
            .build();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDto> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable)
            .map(this::map);
    }
}
