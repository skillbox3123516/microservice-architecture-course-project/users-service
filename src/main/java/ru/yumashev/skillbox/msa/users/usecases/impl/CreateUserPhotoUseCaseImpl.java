package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserPhotoRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.CreateUserPhotoUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CreateUserPhotoUseCaseImpl implements CreateUserPhotoUseCase {

    private final UserRepository userRepository;
    private final UserPhotoRepository userPhotoRepository;

    @Override
    @Transactional
    public void create(UUID userId, MultipartFile file) {
        final User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        String key = userPhotoRepository.save(userId.toString(), file);
        user.setIconUrl(key);
    }
}
