package ru.yumashev.skillbox.msa.users.usecases.exceptions;

import java.util.UUID;

public class UserPhotoNotFoundException extends RuntimeException {
    public UserPhotoNotFoundException(UUID id) {
        super(String.format("Не найдено фото пользователя. %s", id));
    }
}
