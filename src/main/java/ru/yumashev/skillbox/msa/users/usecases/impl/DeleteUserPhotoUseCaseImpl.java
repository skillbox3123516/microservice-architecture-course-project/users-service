package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserPhotoRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.DeleteUserPhotoUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.UUID;

@RequiredArgsConstructor
@Service
public class DeleteUserPhotoUseCaseImpl implements DeleteUserPhotoUseCase {

    private final UserRepository userRepository;
    private final UserPhotoRepository userPhotoRepository;

    @Transactional
    @Override
    public void delete(UUID userId) {
        final User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        if (user.getIconUrl() != null) {
            userPhotoRepository.deleteByObjectName(user.getId().toString());
            user.setIconUrl(null);
        }
    }
}
