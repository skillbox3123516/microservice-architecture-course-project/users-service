package ru.yumashev.skillbox.msa.users.repositories.impl;

import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.StatObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.yumashev.skillbox.msa.users.repositories.UserPhotoRepository;
import ru.yumashev.skillbox.msa.users.usecases.impl.ByteMultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserPhotoRepositoryImpl implements UserPhotoRepository {

    public static final String USER_PHOTO_BUCKET = "user-photo";
    public static final String FILE_NAME_METADATA = "filename";
    private final MinioClient minioClient;

    @Override
    public MultipartFile findByObjectName(String objectName) {

        try (var object = minioClient.getObject(GetObjectArgs.builder()
            .bucket(USER_PHOTO_BUCKET)
            .object(objectName)
            .build())) {
            var stat = minioClient.statObject(StatObjectArgs.builder()
                .bucket(USER_PHOTO_BUCKET)
                .object(objectName)
                .build());
            return new ByteMultipartFile(object.readAllBytes(), stat.userMetadata().get(FILE_NAME_METADATA), stat.contentType());
        } catch (ErrorResponseException | XmlParserException | ServerException | NoSuchAlgorithmException | IOException |
                 InvalidResponseException | InvalidKeyException | InternalException |
                 InsufficientDataException e) {
            throw new UserPhotoRepositoryException(e);
        }
    }

    @Override
    public String save(String objectName, MultipartFile file) {
        final ObjectWriteResponse objectWriteResponse;
        try (
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(file.getBytes())) {
            objectWriteResponse = minioClient.putObject(PutObjectArgs.builder()
                .bucket(USER_PHOTO_BUCKET)
                .object(objectName)
                .contentType(file.getContentType())
                .userMetadata(Map.of(FILE_NAME_METADATA, Optional.ofNullable(file.getOriginalFilename()).orElse(file.getName())))
                .stream(byteArrayInputStream, file.getSize(), -1)
                .build());
            return objectWriteResponse.etag();
        } catch (ErrorResponseException | InsufficientDataException | XmlParserException | ServerException | NoSuchAlgorithmException |
                 InternalException | InvalidKeyException | InvalidResponseException | IOException e) {
            throw new UserPhotoRepositoryException(e);
        }
    }

    @Override
    public void deleteByObjectName(String objectName) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder()
                .bucket(USER_PHOTO_BUCKET)
                .object(objectName)
                .build());
        } catch (ErrorResponseException | XmlParserException | ServerException | NoSuchAlgorithmException | IOException |
                 InvalidResponseException | InvalidKeyException | InternalException | InsufficientDataException e) {
            throw new UserPhotoRepositoryException(e);
        }
    }
}
