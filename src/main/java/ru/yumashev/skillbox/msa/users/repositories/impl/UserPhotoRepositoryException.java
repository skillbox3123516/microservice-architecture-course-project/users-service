package ru.yumashev.skillbox.msa.users.repositories.impl;

public class UserPhotoRepositoryException extends RuntimeException {

    public UserPhotoRepositoryException() {
    }

    public UserPhotoRepositoryException(String message) {
        super(message);
    }

    public UserPhotoRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserPhotoRepositoryException(Throwable cause) {
        super(cause);
    }

    public UserPhotoRepositoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
