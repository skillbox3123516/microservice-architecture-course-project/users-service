package ru.yumashev.skillbox.msa.users.usecases;

import java.util.UUID;

public interface SubscriptionsUseCase {
    void subscribe(UUID subscriberId, UUID subscriptionId);

    void unsubscribe(UUID subscriberId, UUID subscriptionId);
}
