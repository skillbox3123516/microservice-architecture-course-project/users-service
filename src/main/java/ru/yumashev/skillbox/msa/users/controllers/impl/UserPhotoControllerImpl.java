package ru.yumashev.skillbox.msa.users.controllers.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartFile;
import ru.yumashev.skillbox.msa.users.controllers.UserPhotoController;
import ru.yumashev.skillbox.msa.users.usecases.CreateUserPhotoUseCase;
import ru.yumashev.skillbox.msa.users.usecases.DeleteUserPhotoUseCase;
import ru.yumashev.skillbox.msa.users.usecases.GetUserPhotoUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserPhotoNotFoundException;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;


@RequiredArgsConstructor
@Controller
public class UserPhotoControllerImpl implements UserPhotoController {

    private final CreateUserPhotoUseCase createUserPhotoUseCase;
    private final GetUserPhotoUseCase getUserPhotoUseCase;
    private final DeleteUserPhotoUseCase deleteUserPhotoUseCase;

    @Override
    public void createUserPhoto(UUID userId, MultipartFile file) {
        createUserPhotoUseCase.create(userId, file);
    }

    @Override
    public ResponseEntity<InputStreamResource> getUserPhoto(UUID userId) {
        final MultipartFile file = getUserPhotoUseCase.get(userId);
        try {
            return ResponseEntity
                .ok()
                .contentType(Optional.ofNullable(file.getContentType()).map(MediaType::valueOf).orElse(MediaType.APPLICATION_OCTET_STREAM))
                .body(new InputStreamResource(file.getInputStream()));
        } catch (IOException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public void deleteUserPhoto(UUID userId) {
        deleteUserPhotoUseCase.delete(userId);
    }

    @ExceptionHandler({UserNotFoundException.class, UserPhotoNotFoundException.class})
    public ResponseEntity<?> handleAccessDeniedException(Exception ex) {
        return ResponseEntity
            .notFound()
            .build();
    }

}
