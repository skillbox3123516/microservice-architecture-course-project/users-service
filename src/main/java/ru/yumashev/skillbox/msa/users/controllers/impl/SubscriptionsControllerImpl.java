package ru.yumashev.skillbox.msa.users.controllers.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ru.yumashev.skillbox.msa.users.controllers.SubscriptionsController;
import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriberDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriptionDto;
import ru.yumashev.skillbox.msa.users.usecases.GetSubscribersUseCase;
import ru.yumashev.skillbox.msa.users.usecases.GetSubscriptionsUseCase;
import ru.yumashev.skillbox.msa.users.usecases.SubscriptionsUseCase;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class SubscriptionsControllerImpl implements SubscriptionsController {

    private final SubscriptionsUseCase subscriptionsUseCase;
    private final GetSubscriptionsUseCase getSubscriptionsUseCase;
    private final GetSubscribersUseCase getSubscribersUseCase;

    @Override
    public void createSubscription(UUID subscriberId, UUID subscriptionId) {
        subscriptionsUseCase.subscribe(subscriberId, subscriptionId);
    }

    @Override
    public void deleteSubscription(UUID subscriberId, UUID subscriptionId) {
        subscriptionsUseCase.unsubscribe(subscriberId, subscriptionId);
    }

    @Override
    public List<SubscriptionDto> getSubscriptions(UUID subscriberId) {
        return getSubscriptionsUseCase.getAll(subscriberId);
    }

    @Override
    public List<SubscriberDto> getSubscribers(UUID subscriptionId) {
        return getSubscribersUseCase.getAll(subscriptionId);
    }
}
