package ru.yumashev.skillbox.msa.users.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RequestMapping("/users/{userId}/photo")
public interface UserPhotoController {

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(description = "Установить фото пользователя")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "201", description = "OK", content = @Content()),
            @ApiResponse(responseCode = "400", description = "Неправильный формат входных параметров", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void createUserPhoto(@PathVariable UUID userId, @RequestParam("file") MultipartFile file);

    @GetMapping
    @Operation(description = "Получить фото пользователя")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content()),
            @ApiResponse(responseCode = "400", description = "Неправильный формат входных параметров", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Ресурс не найден", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    ResponseEntity<InputStreamResource> getUserPhoto(@PathVariable UUID userId);

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(description = "Удалить фото пользователя")
    @ApiResponses(
        value = {
            @ApiResponse(responseCode = "200", description = "OK",
                content = @Content()),
            @ApiResponse(responseCode = "400", description = "Неправильный формат входных параметров", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Ресурс не найден", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal error", content = @Content())})
    void deleteUserPhoto(@PathVariable UUID userId);

}
