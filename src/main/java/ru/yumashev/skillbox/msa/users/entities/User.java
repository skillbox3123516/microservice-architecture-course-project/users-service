package ru.yumashev.skillbox.msa.users.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "usr")
@Getter
@Setter
@SQLDelete(sql = "UPDATE users_schema.usr SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class User extends UuidSuperEntity {

    @Column(name = "first_name", length = 512)
    private String firstName;

    @Column(name = "last_name", length = 512)
    private String lastName;

    @Column(name = "middle_name", length = 512)
    private String middleName;

    @Column(name = "gender", length = 32)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "birthdate", length = 32)
    private LocalDate birthDate;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Lob
    @Column(name = "icon_url")
    private String iconUrl;

    @Lob
    @Column(name = "about")
    private String about;

    @Column(name = "nickname", length = 256, nullable = false)
    private String nickname;

    @Column(name = "email", length = 256, nullable = false)
    private String email;

    @Column(name = "phone", length = 256, nullable = false)
    private String phone;

    @OneToMany(mappedBy = "subscriber")
    private List<Subscription> subscriptions = new ArrayList<>();

    @OneToMany(mappedBy = "subscription")
    private List<Subscription> subscribers = new ArrayList<>();

    @Column
    private boolean deleted = Boolean.FALSE;
}
