package ru.yumashev.skillbox.msa.users.usecases;

import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriberDto;

import java.util.List;
import java.util.UUID;

public interface GetSubscribersUseCase {
    List<SubscriberDto> getAll(UUID subscriptionId);
}
