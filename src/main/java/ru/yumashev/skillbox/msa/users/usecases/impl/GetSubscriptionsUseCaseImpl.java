package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriptionDto;
import ru.yumashev.skillbox.msa.users.repositories.SubscriptionRepository;
import ru.yumashev.skillbox.msa.users.usecases.GetSubscriptionsUseCase;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GetSubscriptionsUseCaseImpl implements GetSubscriptionsUseCase {

    private final SubscriptionRepository subscriptionRepository;

    @Override
    @Transactional
    public List<SubscriptionDto> getAll(UUID subscriberId) {
        return subscriptionRepository.findBySubscriberId(subscriberId)
            .stream()
            .map(subscription -> SubscriptionDto.builder()
                .id(subscription.getSubscription().getId())
                .nickname(subscription.getSubscription().getNickname())
                .build())
            .collect(Collectors.toList());
    }
}
