package ru.yumashev.skillbox.msa.users.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.yumashev.skillbox.msa.users.entities.HardSkill;

import java.util.UUID;

@Repository
public interface HardSkillRepository extends JpaRepository<HardSkill, UUID> {
}