package ru.yumashev.skillbox.msa.users.usecases.exceptions;

import java.util.UUID;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(UUID id) {
        super(String.format("Не найден пользователь. %s", id));
    }
}
