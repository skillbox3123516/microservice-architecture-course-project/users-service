package ru.yumashev.skillbox.msa.users.usecases;

import java.util.UUID;

public interface DeleteUserPhotoUseCase {
    void delete(UUID userId);
}
