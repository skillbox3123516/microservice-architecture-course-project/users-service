package ru.yumashev.skillbox.msa.users.usecases;

import org.springframework.core.io.InputStreamSource;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface GetUserPhotoUseCase {
    MultipartFile get(UUID userId);
}
