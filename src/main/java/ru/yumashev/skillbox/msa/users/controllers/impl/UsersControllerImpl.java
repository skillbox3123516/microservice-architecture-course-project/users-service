package ru.yumashev.skillbox.msa.users.controllers.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import ru.yumashev.skillbox.msa.users.controllers.UsersController;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UpdateUserDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UserDto;
import ru.yumashev.skillbox.msa.users.usecases.CreateUserUseCase;
import ru.yumashev.skillbox.msa.users.usecases.DeleteUserUseCase;
import ru.yumashev.skillbox.msa.users.usecases.GetUsersUseCase;
import ru.yumashev.skillbox.msa.users.usecases.UpdateUserUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UsersControllerImpl implements UsersController {

    private final CreateUserUseCase createUserUseCase;
    private final GetUsersUseCase getUsersUseCase;
    private final DeleteUserUseCase deleteUserUseCase;
    private final UpdateUserUseCase updateUserUseCase;

    @Override
    public UUID createUser(@Valid CreateUserDto createUserDto) {
        return createUserUseCase.create(createUserDto).getId();
    }

    @Override
    public void updateUser(UUID id, @Valid UpdateUserDto updateUserDto) {
        updateUserUseCase.update(id, updateUserDto);
    }

    @Override
    public void deleteUser(UUID id) {
        deleteUserUseCase.delete(id);
    }

    @Override
    public UserDto getUser(UUID id) {
        return getUsersUseCase.getById(id);
    }

    @Override
    public Page<UserDto> getAllUsers(Pageable pageable) {
        return getUsersUseCase.getAllUsers(Optional.ofNullable(pageable).orElseGet(() -> Pageable.ofSize(100)));
    }

    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity<?> handleAccessDeniedException(UserNotFoundException ex) {
        return ResponseEntity
            .notFound()
            .build();
    }

}
