package ru.yumashev.skillbox.msa.users.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "hard_skill")
@Getter
@Setter
public class HardSkill extends UuidSuperEntity {
    @Column(name = "name", nullable = false, length = 256)
    private String name;
}
