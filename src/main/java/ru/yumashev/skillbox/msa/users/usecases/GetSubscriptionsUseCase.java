package ru.yumashev.skillbox.msa.users.usecases;

import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriptionDto;

import java.util.List;
import java.util.UUID;

public interface GetSubscriptionsUseCase {
    List<SubscriptionDto> getAll(UUID subscriberId);
}
