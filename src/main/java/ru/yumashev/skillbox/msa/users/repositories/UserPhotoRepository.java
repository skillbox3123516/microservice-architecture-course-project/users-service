package ru.yumashev.skillbox.msa.users.repositories;

import org.springframework.web.multipart.MultipartFile;

public interface UserPhotoRepository {

    MultipartFile findByObjectName(String objectName);

    String save(String objectName, MultipartFile file);

    void deleteByObjectName(String objectName);
}
