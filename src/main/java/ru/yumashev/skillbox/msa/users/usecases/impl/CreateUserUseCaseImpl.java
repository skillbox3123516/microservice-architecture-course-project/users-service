package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yumashev.skillbox.msa.users.controllers.dto.CityDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.CityRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.CreateUserUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.CityNotFoundException;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CreateUserUseCaseImpl implements CreateUserUseCase {

    private final UserRepository userRepository;
    private final CityRepository cityRepository;

    @Override
    @Transactional
    public User create(CreateUserDto createUserDto) {
        final User user = new User();
        user.setNickname(createUserDto.getNickname());
        user.setEmail(createUserDto.getEmail());
        user.setPhone(createUserDto.getPhone());
        user.setFirstName(createUserDto.getFirstName());
        user.setLastName(createUserDto.getLastName());
        user.setMiddleName(createUserDto.getMiddleName());
        user.setGender(createUserDto.getGender());
        user.setAbout(createUserDto.getAbout());
        Optional.ofNullable(createUserDto.getCity())
            .map(CityDto::getId)
            .map(cityId -> cityRepository.findById(cityId).orElseThrow(() -> new CityNotFoundException(cityId)))
            .ifPresent(user::setCity);
        user.setBirthDate(createUserDto.getBirthDate());
        return userRepository.save(user);
    }
}
