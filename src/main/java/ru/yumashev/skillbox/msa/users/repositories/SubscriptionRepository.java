package ru.yumashev.skillbox.msa.users.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import ru.yumashev.skillbox.msa.users.entities.Subscription;
import ru.yumashev.skillbox.msa.users.entities.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, UUID> {
    Optional<Subscription> findBySubscriberAndSubscription(@NonNull User subscriber, @NonNull User subscription);

    @Query("select s from Subscription s where s.subscriber.id = :subscriberId")
    List<Subscription> findBySubscriberId(UUID subscriberId);

    @Query("select s from Subscription s where s.subscription.id = :subscriptionId")
    List<Subscription> findBySubscriptionId(UUID subscriptionId);
}