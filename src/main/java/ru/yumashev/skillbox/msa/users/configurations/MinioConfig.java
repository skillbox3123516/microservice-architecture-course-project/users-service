package ru.yumashev.skillbox.msa.users.configurations;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.yumashev.skillbox.msa.users.repositories.impl.UserPhotoRepositoryImpl;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Configuration
public class MinioConfig {

    @Bean
    public MinioClient minioClient(MinioProperties minioProperties)
        throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException,
        InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        final MinioClient minioClient = MinioClient.builder()
            .endpoint(minioProperties.getHost())
            .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey())
            .build();
        if (!minioClient.bucketExists(BucketExistsArgs.builder()
            .bucket(UserPhotoRepositoryImpl.USER_PHOTO_BUCKET)
            .build())) {
            minioClient.makeBucket(MakeBucketArgs.builder()
                .bucket(UserPhotoRepositoryImpl.USER_PHOTO_BUCKET)
                .build());
        }
        return minioClient;
    }

}
