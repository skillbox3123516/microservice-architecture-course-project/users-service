package ru.yumashev.skillbox.msa.users.usecases.exceptions;

import java.util.UUID;

public class CityNotFoundException extends RuntimeException {
    public CityNotFoundException(UUID cityId) {
        super(String.format("Город не найден. %s", cityId));
    }
}
