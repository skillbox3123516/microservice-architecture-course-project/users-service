package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.DeleteUserUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DeleteUserUseCaseImpl implements DeleteUserUseCase {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public void delete(UUID id) {
        userRepository.findById(id)
            .ifPresentOrElse(userRepository::delete,
                () -> {
                    throw new UserNotFoundException(id);
                });
    }
}
