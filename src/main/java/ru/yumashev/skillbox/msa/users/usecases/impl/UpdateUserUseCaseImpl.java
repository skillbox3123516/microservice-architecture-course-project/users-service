package ru.yumashev.skillbox.msa.users.usecases.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yumashev.skillbox.msa.users.controllers.dto.CityDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UpdateUserDto;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.CityRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.UpdateUserUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.CityNotFoundException;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UpdateUserUseCaseImpl implements UpdateUserUseCase {

    private final UserRepository userRepository;
    private final CityRepository cityRepository;

    @Override
    @Transactional
    public void update(UUID id, UpdateUserDto updateUserDto) {
        final User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
        user.setNickname(updateUserDto.getNickname());
        user.setEmail(updateUserDto.getEmail());
        user.setPhone(updateUserDto.getPhone());
        user.setFirstName(updateUserDto.getFirstName());
        user.setLastName(updateUserDto.getLastName());
        user.setMiddleName(updateUserDto.getMiddleName());
        user.setGender(updateUserDto.getGender());
        user.setAbout(updateUserDto.getAbout());
        Optional.ofNullable(updateUserDto.getCity())
            .map(CityDto::getId)
            .map(cityId -> cityRepository.findById(cityId).orElseThrow(() -> new CityNotFoundException(cityId)))
            .ifPresent(user::setCity);
        user.setBirthDate(updateUserDto.getBirthDate());
    }
}
