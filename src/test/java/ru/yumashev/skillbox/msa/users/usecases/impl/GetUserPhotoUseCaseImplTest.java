package ru.yumashev.skillbox.msa.users.usecases.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserPhotoRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.repositories.impl.UserPhotoRepositoryException;
import ru.yumashev.skillbox.msa.users.usecases.GetUserPhotoUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserPhotoNotFoundException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.UUID;

class GetUserPhotoUseCaseImplTest {
    @Test
    void getSuccess() throws IOException {
        final User user = new User();
        user.setId(UUID.randomUUID());
        user.setIconUrl("789");
        final MockMultipartFile exceptedFile = getFile();
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final UserPhotoRepository userPhotoRepository = Mockito.mock(UserPhotoRepository.class);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        Mockito.when(userPhotoRepository.findByObjectName(user.getId().toString())).thenReturn(exceptedFile);
        final GetUserPhotoUseCase getUserPhotoUseCase = new GetUserPhotoUseCaseImpl(userRepository, userPhotoRepository);
        final MultipartFile file = getUserPhotoUseCase.get(user.getId());
        Assertions.assertEquals(exceptedFile, file);
    }

    @Test
    void getSuccessNotFound() throws IOException {
        final User user = new User();
        user.setId(UUID.randomUUID());
        user.setIconUrl(null);
        final MockMultipartFile exceptedFile = getFile();
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final UserPhotoRepository userPhotoRepository = Mockito.mock(UserPhotoRepository.class);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        Mockito.when(userPhotoRepository.findByObjectName(user.getId().toString())).thenReturn(exceptedFile);
        final GetUserPhotoUseCase getUserPhotoUseCase = new GetUserPhotoUseCaseImpl(userRepository, userPhotoRepository);
        Assertions.assertThrowsExactly(UserPhotoNotFoundException.class, () -> getUserPhotoUseCase.get(user.getId()));
    }

    @Test
    void getSuccessNotFoundCauseRepositoryException() throws IOException {
        final User user = new User();
        user.setId(UUID.randomUUID());
        user.setIconUrl("123456789");
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final UserPhotoRepository userPhotoRepository = Mockito.mock(UserPhotoRepository.class);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        Mockito.when(userPhotoRepository.findByObjectName(user.getId().toString())).thenThrow(new UserPhotoRepositoryException());
        final GetUserPhotoUseCase getUserPhotoUseCase = new GetUserPhotoUseCaseImpl(userRepository, userPhotoRepository);
        Assertions.assertThrowsExactly(UserPhotoNotFoundException.class, () -> getUserPhotoUseCase.get(user.getId()));
    }


    MockMultipartFile getFile() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("test.png").getFile());
        return new MockMultipartFile(file.getName(), file.getName(), Files.probeContentType(file.toPath()),
            Files.readAllBytes(file.toPath()));
    }

}