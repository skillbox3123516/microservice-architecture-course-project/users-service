package ru.yumashev.skillbox.msa.users.usecases.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserPhotoRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;

import java.util.Optional;
import java.util.UUID;

class DeleteUserPhotoUseCaseImplTest {

    @Test
    void delete() {
        final User user = new User();
        user.setId(UUID.randomUUID());
        user.setIconUrl("789");
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final UserPhotoRepository userPhotoRepository = Mockito.mock(UserPhotoRepository.class);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        final DeleteUserPhotoUseCaseImpl deleteUserPhotoUseCase = new DeleteUserPhotoUseCaseImpl(userRepository, userPhotoRepository);
        deleteUserPhotoUseCase.delete(user.getId());
        Mockito.verify(userPhotoRepository).deleteByObjectName(user.getId().toString());
        Assertions.assertNull(user.getIconUrl());
    }
}