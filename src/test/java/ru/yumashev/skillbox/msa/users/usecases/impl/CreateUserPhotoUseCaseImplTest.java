package ru.yumashev.skillbox.msa.users.usecases.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserPhotoRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.UUID;

class CreateUserPhotoUseCaseImplTest {

    @Test
    void create() throws IOException {
        final User user = new User();
        user.setId(UUID.randomUUID());
        MockMultipartFile file = getFile();
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final UserPhotoRepository userPhotoRepository = Mockito.mock(UserPhotoRepository.class);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        final String objectKey = "789";
        Mockito.when(userPhotoRepository.save(user.getId().toString(), file)).thenReturn(objectKey);
        final CreateUserPhotoUseCaseImpl createUserPhotoUseCase = new CreateUserPhotoUseCaseImpl(userRepository, userPhotoRepository);
        createUserPhotoUseCase.create(user.getId(), file);
        Assertions.assertEquals(objectKey, user.getIconUrl());
    }

    MockMultipartFile getFile() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("test.png").getFile());
        return new MockMultipartFile(file.getName(), file.getName(), Files.probeContentType(file.toPath()),
            Files.readAllBytes(file.toPath()));
    }
}