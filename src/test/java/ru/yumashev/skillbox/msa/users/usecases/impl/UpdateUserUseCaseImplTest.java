package ru.yumashev.skillbox.msa.users.usecases.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.yumashev.skillbox.msa.users.controllers.dto.CityDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UpdateUserDto;
import ru.yumashev.skillbox.msa.users.entities.City;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.CityRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.UpdateUserUseCase;

import java.util.Optional;
import java.util.UUID;

class UpdateUserUseCaseImplTest {

    @Test
    void update() {
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final CityRepository cityRepository = Mockito.mock(CityRepository.class);
        City city = new City();
        city.setId(UUID.randomUUID());
        User exceptedUser = new User();
        exceptedUser.setEmail("email@email.com");
        exceptedUser.setNickname("nickname");
        exceptedUser.setPhone("phone");
        exceptedUser.setId(UUID.randomUUID());
        exceptedUser.setCity(city);
        Mockito.when(userRepository.findById(exceptedUser.getId())).thenReturn(Optional.of(exceptedUser));
        Mockito.when(cityRepository.findById(city.getId())).thenReturn(Optional.of(city));
        final UpdateUserUseCase userUseCase = new UpdateUserUseCaseImpl(userRepository, cityRepository);
        UpdateUserDto updateUserDto = UpdateUserDto.builder()
            .email(exceptedUser.getEmail())
            .nickname(exceptedUser.getNickname())
            .phone("new phone")
            .city(CityDto.builder().id(city.getId()).build())
            .build();
        userUseCase.update(exceptedUser.getId(), updateUserDto);
        Assertions.assertEquals(updateUserDto.getPhone(), exceptedUser.getPhone());
        Mockito.verify(userRepository, Mockito.atMostOnce()).save(exceptedUser);
        Mockito.clearInvocations(userRepository, cityRepository);
    }
}