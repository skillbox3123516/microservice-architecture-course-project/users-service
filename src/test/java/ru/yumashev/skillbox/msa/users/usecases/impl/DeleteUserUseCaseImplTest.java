package ru.yumashev.skillbox.msa.users.usecases.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.DeleteUserUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.Optional;
import java.util.UUID;

class DeleteUserUseCaseImplTest {

    @Test
    void deleteSuccess() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UUID id = UUID.randomUUID();
        User user = new User();
        user.setId(id);
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        Mockito.doNothing().when(userRepository).delete(userCaptor.capture());
        final DeleteUserUseCase deleteUserUseCase = new DeleteUserUseCaseImpl(userRepository);
        deleteUserUseCase.delete(id);
        Mockito.verify(userRepository, Mockito.atMostOnce()).delete(Mockito.any());
        Assertions.assertEquals(user, userCaptor.getValue());
    }


    @Test
    void deleteErrorUserNotFound() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        final DeleteUserUseCase deleteUserUseCase = new DeleteUserUseCaseImpl(userRepository);
        Assertions.assertThrowsExactly(UserNotFoundException.class, () -> {
            deleteUserUseCase.delete(UUID.randomUUID());
        });
        Mockito.verify(userRepository, Mockito.never()).delete(Mockito.any());
    }

}