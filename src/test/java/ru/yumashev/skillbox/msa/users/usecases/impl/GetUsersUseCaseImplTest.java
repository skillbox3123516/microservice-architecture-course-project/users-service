package ru.yumashev.skillbox.msa.users.usecases.impl;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import ru.yumashev.skillbox.msa.users.controllers.dto.UserDto;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.GetUsersUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.Optional;
import java.util.UUID;

class GetUsersUseCaseImplTest {

    @Test
    void getById() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UUID id = UUID.randomUUID();
        User user = new User();
        user.setId(id);
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        GetUsersUseCase getUsersUseCase = new GetUsersUseCaseImpl(userRepository);
        final UserDto byId = getUsersUseCase.getById(id);
        Assertions.assertEquals(user.getId(), byId.getId());
    }

    @Test
    void getByIdUserNotFoundException() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UUID id = UUID.randomUUID();
        User user = new User();
        user.setId(id);
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        GetUsersUseCase getUsersUseCase = new GetUsersUseCaseImpl(userRepository);
        Assertions.assertThrowsExactly(UserNotFoundException.class, () -> getUsersUseCase.getById(UUID.randomUUID()));
    }

    @Test
    void getAllUsers() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        UUID id = UUID.randomUUID();
        User user = new User();
        user.setId(id);
        final Pageable pageable = Pageable.ofSize(100);
        Mockito.when(userRepository.findAll(pageable)).thenReturn(new PageImpl<>(Lists.list(user)));
        GetUsersUseCase getUsersUseCase = new GetUsersUseCaseImpl(userRepository);
        final Page<UserDto> byId = getUsersUseCase.getAllUsers(pageable);
        Assertions.assertEquals(1, byId.getSize());
    }

}