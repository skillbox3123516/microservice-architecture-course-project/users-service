package ru.yumashev.skillbox.msa.users.usecases.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import ru.yumashev.skillbox.msa.users.entities.Subscription;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.SubscriptionRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.SubscriptionsUseCase;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.util.Optional;
import java.util.UUID;

class SubscriptionsUseCaseImplTest {

    @Test
    void subscribe() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        SubscriptionRepository subscriptionRepository = Mockito.mock(SubscriptionRepository.class);
        UUID subscriberId = UUID.randomUUID();
        User subscriber = new User();
        subscriber.setId(subscriberId);
        UUID subscriptionId = UUID.randomUUID();
        User subscription = new User();
        subscription.setId(subscriptionId);
        final SubscriptionsUseCase subscriptionsUseCase = new SubscriptionsUseCaseImpl(userRepository, subscriptionRepository);
        Mockito.when(userRepository.findById(subscriberId)).thenReturn(Optional.of(subscriber));
        Mockito.when(userRepository.findById(subscriptionId)).thenReturn(Optional.of(subscription));
        Mockito.when(subscriptionRepository.findBySubscriberAndSubscription(subscription, subscription)).thenReturn(Optional.empty());
        ArgumentCaptor<Subscription> subscriptionArgumentCaptor = ArgumentCaptor.forClass(Subscription.class);
        final Subscription savedSub = new Subscription();
        Mockito.when(subscriptionRepository.save(subscriptionArgumentCaptor.capture())).thenReturn(savedSub);
        subscriptionsUseCase.subscribe(subscriberId, subscriptionId);
        Mockito.verify(subscriptionRepository, Mockito.atMostOnce()).save(Mockito.any());
        final Subscription value = subscriptionArgumentCaptor.getValue();
        Assertions.assertEquals(subscriber, value.getSubscriber());
        Assertions.assertEquals(subscription, value.getSubscription());
    }

    @Test
    void subscribeUserNitFoundError() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        SubscriptionRepository subscriptionRepository = Mockito.mock(SubscriptionRepository.class);
        UUID subscriberId = UUID.randomUUID();
        User subscriber = new User();
        subscriber.setId(subscriberId);
        UUID subscriptionId = UUID.randomUUID();
        User subscription = new User();
        subscription.setId(subscriptionId);
        final SubscriptionsUseCase subscriptionsUseCase = new SubscriptionsUseCaseImpl(userRepository, subscriptionRepository);
        Mockito.when(userRepository.findById(subscriberId)).thenReturn(Optional.of(subscriber));
        Mockito.when(userRepository.findById(subscriptionId)).thenReturn(Optional.of(subscription));
        Mockito.when(subscriptionRepository.findBySubscriberAndSubscription(subscription, subscription)).thenReturn(Optional.empty());
        Assertions.assertThrowsExactly(UserNotFoundException.class,
            () -> subscriptionsUseCase.subscribe(UUID.randomUUID(), UUID.randomUUID()));
        Mockito.verify(subscriptionRepository, Mockito.never()).save(Mockito.any());
    }


    @Test
    void unsubscribe() {
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        SubscriptionRepository subscriptionRepository = Mockito.mock(SubscriptionRepository.class);
        UUID subscriberId = UUID.randomUUID();
        User subscriber = new User();
        subscriber.setId(subscriberId);
        UUID subscriptionId = UUID.randomUUID();
        User subscription = new User();
        subscription.setId(subscriptionId);
        final SubscriptionsUseCase subscriptionsUseCase = new SubscriptionsUseCaseImpl(userRepository, subscriptionRepository);
        Mockito.when(userRepository.findById(subscriberId)).thenReturn(Optional.of(subscriber));
        Mockito.when(userRepository.findById(subscriptionId)).thenReturn(Optional.of(subscription));
        final Subscription savedSub = new Subscription();
        savedSub.setSubscriber(subscriber);
        savedSub.setSubscription(subscription);
        Mockito.when(subscriptionRepository.findBySubscriberAndSubscription(subscriber, subscription)).thenReturn(Optional.of(savedSub));
        ArgumentCaptor<Subscription> subscriptionArgumentCaptor = ArgumentCaptor.forClass(Subscription.class);
        Mockito.doNothing().when(subscriptionRepository).delete(subscriptionArgumentCaptor.capture());
        subscriptionsUseCase.unsubscribe(subscriberId, subscriptionId);
        Mockito.verify(subscriptionRepository, Mockito.atMostOnce()).delete(savedSub);
        final Subscription value = subscriptionArgumentCaptor.getValue();
        Assertions.assertEquals(savedSub, value);
    }
}