package ru.yumashev.skillbox.msa.users.usecases.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.yumashev.skillbox.msa.users.controllers.dto.CityDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;
import ru.yumashev.skillbox.msa.users.entities.City;
import ru.yumashev.skillbox.msa.users.entities.User;
import ru.yumashev.skillbox.msa.users.repositories.CityRepository;
import ru.yumashev.skillbox.msa.users.repositories.UserRepository;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.CityNotFoundException;

import javax.persistence.PersistenceException;
import java.util.Optional;
import java.util.UUID;

class CreateUserUseCaseImplTest {

    @Test
    void createUserSuccess() {
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final CityRepository cityRepository = Mockito.mock(CityRepository.class);
        final City city = new City();
        city.setId(UUID.randomUUID());
        User exceptedUser = new User();
        exceptedUser.setEmail("email@email.com");
        exceptedUser.setNickname("nickname");
        exceptedUser.setPhone("phone");
        exceptedUser.setId(UUID.randomUUID());
        exceptedUser.setCity(city);
        Mockito.when(userRepository.save(Mockito.any(User.class)))
            .thenAnswer(invocation -> {
                final User argument = invocation.getArgument(0);
                Assertions.assertEquals(city, argument.getCity());
                Assertions.assertEquals(exceptedUser.getNickname(), argument.getNickname());
                return exceptedUser;
            });
        Mockito.when(cityRepository.findById(city.getId())).thenReturn(Optional.of(city));
        final CreateUserUseCaseImpl createUserUseCase = new CreateUserUseCaseImpl(userRepository, cityRepository);
        CreateUserDto createUserDto = CreateUserDto.builder()
            .email(exceptedUser.getEmail())
            .nickname(exceptedUser.getNickname())
            .phone(exceptedUser.getPhone())
            .city(CityDto.builder().id(city.getId()).build())
            .build();
        final User actualUser = createUserUseCase.create(createUserDto);
        Assertions.assertEquals(exceptedUser, actualUser);
        Mockito.clearInvocations(userRepository, cityRepository);
    }


    @Test
    void createUserError() {
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final CityRepository cityRepository = Mockito.mock(CityRepository.class);
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenThrow(PersistenceException.class);
        final CreateUserUseCaseImpl createUserUseCase = new CreateUserUseCaseImpl(userRepository, cityRepository);
        CreateUserDto createUserDto = CreateUserDto.builder()
            .build();
        Assertions.assertThrowsExactly(PersistenceException.class, () -> createUserUseCase.create(createUserDto));
        Mockito.clearInvocations(userRepository, cityRepository);
    }

    @Test
    void createCityError() {
        final UserRepository userRepository = Mockito.mock(UserRepository.class);
        final CityRepository cityRepository = Mockito.mock(CityRepository.class);
        final City city = new City();
        city.setId(UUID.randomUUID());
        final User exceptedUser = new User();
        exceptedUser.setEmail("email@email.com");
        exceptedUser.setNickname("nickname");
        exceptedUser.setPhone("phone");
        exceptedUser.setId(UUID.randomUUID());
        exceptedUser.setCity(city);
        Mockito.when(cityRepository.findById(city.getId())).thenReturn(Optional.of(city));
        final CreateUserUseCaseImpl createUserUseCase = new CreateUserUseCaseImpl(userRepository, cityRepository);
        CreateUserDto createUserDto = CreateUserDto.builder()
            .email(exceptedUser.getEmail())
            .nickname(exceptedUser.getNickname())
            .phone(exceptedUser.getPhone())
            .city(CityDto.builder().id(UUID.randomUUID()).build())
            .build();
        Assertions.assertThrowsExactly(CityNotFoundException.class, () -> createUserUseCase.create(createUserDto));
        Mockito.clearInvocations(userRepository, cityRepository);
    }

}