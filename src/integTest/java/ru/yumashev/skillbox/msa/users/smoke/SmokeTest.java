package ru.yumashev.skillbox.msa.users.smoke;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.yumashev.skillbox.msa.users.controllers.AbstractControllerTestContainersTest;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UserDto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SmokeTest extends AbstractControllerTestContainersTest {

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void createAndGetUser() throws JsonProcessingException {
        //given
        String url = "http://localhost:" + port;
        final CreateUserDto createUserDto = CreateUserDto.builder()
            .email("email@email.com")
            .phone("phone")
            .nickname("nickname")
            .build();
        //when
        var createResponse = restTemplate.postForEntity(url + "/users/", createUserDto, String.class);
        // then
        assertEquals(201, createResponse.getStatusCodeValue());

        var userId = objectMapper.readValue(createResponse.getBody(), String.class);
        var getResponse = restTemplate.getForEntity(url + "/users/" + userId, UserDto.class);
        assertEquals(200, getResponse.getStatusCodeValue());
        assertNotNull(getResponse.getBody());
        assertEquals(userId, getResponse.getBody().getId().toString());
    }


}
