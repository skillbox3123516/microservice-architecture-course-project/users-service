package ru.yumashev.skillbox.msa.users.regress;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.instancio.Instancio;
import org.instancio.Select;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.yumashev.skillbox.msa.users.AbstractTestContainersTest;
import ru.yumashev.skillbox.msa.users.controllers.dto.CityDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.SubscriptionDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.UserDto;
import ru.yumashev.skillbox.msa.users.usecases.exceptions.UserNotFoundException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegressScenarioTest extends AbstractTestContainersTest {

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;
    private String url;

    @BeforeAll
    static void beforeAll() {
        objectMapper.findAndRegisterModules();
    }

    @BeforeEach
    void setUp() {
        this.url = "http://localhost:" + port;
    }

    /**
     * Сценарий тестирования:
     * Создать двух пользователей.
     * Установить фото пользователю
     * Успешно получить их по ID.
     * Провести поиск пользователей.
     * Подписать одного пользователя на другого.
     * Проверить изменившиеся данные.
     * Удалить подписку.
     * Проверить изменившиеся данные.
     * Проверить частичное изменение данных.
     * Проверить изменившиеся данные.
     * Удалить пользователей.
     * Получить ошибку 404 по ID пользователей.
     *
     * @throws JsonProcessingException
     */
    @Test
    void scenarioTest() throws IOException {
        // create users
        UUID firstUserId = createUser();
        UUID secondUserId = createUser();
        Assertions.assertNotNull(firstUserId);
        Assertions.assertNotNull(secondUserId);

        UserDto firstUser = getUser(firstUserId);
        UserDto secondUser = getUser(secondUserId);
        Assertions.assertEquals(firstUserId, firstUser.getId());
        Assertions.assertEquals(secondUserId, secondUser.getId());

        setPhoto(firstUserId, "test.png");

        subscribe(firstUserId, secondUserId);
        List<UUID> subscriptions = getSubscriptions(firstUserId);
        Assertions.assertTrue(subscriptions.contains(secondUserId));

        unsubscribe(firstUserId, secondUserId);
        subscriptions = getSubscriptions(firstUserId);
        Assertions.assertTrue(subscriptions.isEmpty());

        final String newFirstName = "new First Name";
        assertNotEquals(newFirstName, firstUser.getFirstName());
        firstUser.setFirstName(newFirstName);
        updateUser(firstUserId, firstUser);
        firstUser = getUser(firstUserId);
        assertEquals(newFirstName, firstUser.getFirstName());

        deleteUser(firstUserId);
        deleteUser(secondUserId);

        Assertions.assertThrowsExactly(UserNotFoundException.class, () -> getUser(firstUserId));
        Assertions.assertThrowsExactly(UserNotFoundException.class, () -> getUser(firstUserId));

    }

    private void setPhoto(UUID firstUserId, String image) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", getTestFile(image));
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        final RestTemplate restTemplate1 = new RestTemplate();
        restTemplate1.postForEntity(url + "/users/" + firstUserId + "/photo", requestEntity, String.class);
    }

    Resource getTestFile(String image) {
        return new ClassPathResource(image);
    }

    private List<UUID> getSubscriptions(UUID userId) {
        final SubscriptionDto[] body =
            restTemplate.getForEntity(url + "/subscriptions/subscriptions/" + userId, SubscriptionDto[].class).getBody();
        Assertions.assertNotNull(body);
        return Arrays.stream(body)
            .map(SubscriptionDto::getId)
            .collect(Collectors.toList());
    }

    private void unsubscribe(UUID subscriber, UUID subscription) {
        restTemplate.delete(url + "/subscriptions/" + subscriber + "/to/" + subscription);
    }

    private void subscribe(UUID subscriber, UUID subscription) {
        restTemplate.postForEntity(url + "/subscriptions/" + subscriber + "/to/" + subscription, null, Void.class);
    }

    private UserDto getUser(UUID userId) {
        var getResponse = restTemplate.getForEntity(url + "/users/" + userId, UserDto.class);
        if (getResponse.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
            throw new UserNotFoundException(userId);
        }

        return getResponse.getBody();
    }

    private UUID createUser() throws JsonProcessingException {
        CityDto city = CityDto.builder()
            .id(UUID.fromString("6a3bf316-5110-485f-81d0-eb221e76297b"))
            .name("Москва")
            .build();
        CreateUserDto createUserDto = Instancio.of(CreateUserDto.class)
            .set(Select.field(CreateUserDto::getCity), city)
            .create();

        //when
        var createResponse = restTemplate.postForEntity(url + "/users/", createUserDto, String.class);
        // then
        assertEquals(201, createResponse.getStatusCodeValue());
        var userId = objectMapper.readValue(createResponse.getBody(), String.class);
        return UUID.fromString(userId);
    }

    private void updateUser(UUID userId, UserDto user) {
        restTemplate.put(url + "/users/" + userId, user, Void.class);
    }

    private void deleteUser(UUID userId) {
        restTemplate.delete(url + "/users/" + userId);
    }

}
