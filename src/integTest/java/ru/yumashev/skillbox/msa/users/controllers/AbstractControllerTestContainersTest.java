package ru.yumashev.skillbox.msa.users.controllers;

import org.instancio.Instancio;
import org.instancio.Select;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.yumashev.skillbox.msa.users.AbstractTestContainersTest;
import ru.yumashev.skillbox.msa.users.controllers.dto.CityDto;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;

import java.util.UUID;

public abstract class AbstractControllerTestContainersTest extends AbstractTestContainersTest {

    @Autowired
    protected MockMvc mockMvc;

    protected String createUser(String nickname) throws Exception {
        CityDto city = CityDto.builder()
            .id(UUID.fromString("6a3bf316-5110-485f-81d0-eb221e76297b"))
            .name("Москва")
            .build();
        CreateUserDto createUserDto = Instancio.of(CreateUserDto.class)
            .set(Select.field(CreateUserDto::getNickname), nickname)
            .set(Select.field(CreateUserDto::getCity), city)
            .create();

        //when create
        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createUserDto)))
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andReturn();
        //then
        final String id = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), String.class);
        Assertions.assertNotNull(id);
        return id;
    }
}
