package ru.yumashev.skillbox.msa.users.controllers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

class UserPhotoControllerTest extends AbstractControllerTestContainersTest {

    @Test
    void createGetAndDeleteUserPhotoTest() throws Exception {
        final String testUser = createUser("testUser");
        final MockMultipartFile file = getFile();
        mockMvc.perform(MockMvcRequestBuilders.multipart("/users/{userId}/photo", testUser)
                .file(file))
            .andExpect(MockMvcResultMatchers.status().isCreated());
        final MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/users/{userId}/photo", testUser)
            ).andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();
        final byte[] bytes = file.getBytes();
        final byte[] contentAsByteArray = mvcResult.getResponse().getContentAsByteArray();
        Assertions.assertEquals(bytes.length, contentAsByteArray.length);

        mockMvc.perform(
            MockMvcRequestBuilders.delete("/users/{userId}/photo", testUser)
        ).andExpect(MockMvcResultMatchers.status().isOk());

        mockMvc.perform(
            MockMvcRequestBuilders.get("/users/{userId}/photo", testUser)
        ).andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    MockMultipartFile getFile() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("test.png").getFile());
        return new MockMultipartFile("file", file.getName(), Files.probeContentType(file.toPath()),
            Files.readAllBytes(file.toPath()));
    }

}
