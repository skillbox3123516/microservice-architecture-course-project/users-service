package ru.yumashev.skillbox.msa.users.controllers;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


class SubscriptionsControllerTest extends AbstractControllerTestContainersTest {

    @Test
    void createSubscriptionTestSuccess() throws Exception {
        //given
        final String subscriber = createUser("Subscriber");
        final String subscription = createUser("Subscription");

        // when subscribe
        mockMvc.perform(MockMvcRequestBuilders.post("/subscriptions/" + subscriber + "/to/" + subscription)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk());

        // when subscriptions
        mockMvc.perform(MockMvcRequestBuilders.get("/subscriptions/subscriptions/" + subscriber)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.equalTo(subscription)));

        // when subscribers
        mockMvc.perform(MockMvcRequestBuilders.get("/subscriptions/subscribers/" + subscription)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.equalTo(subscriber)));


        // when unsubscribe
        mockMvc.perform(MockMvcRequestBuilders.delete("/subscriptions/" + subscriber + "/to/" + subscription)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk());

        // when subscriptions
        mockMvc.perform(MockMvcRequestBuilders.get("/subscriptions/subscriptions/" + subscriber)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(0)));

        // when subscribers
        mockMvc.perform(MockMvcRequestBuilders.get("/subscriptions/subscribers/" + subscription)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(0)));

    }

}