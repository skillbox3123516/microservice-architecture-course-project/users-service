package ru.yumashev.skillbox.msa.users.controllers;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.yumashev.skillbox.msa.users.controllers.dto.CreateUserDto;

class UsersControllerTest extends AbstractControllerTestContainersTest {

    @Test
    void applicationContextStartedSuccessfullyTest(ApplicationContext context) throws Exception {
        Assertions.assertNotNull(context);
        // health-check
        mockMvc.perform(MockMvcRequestBuilders.get("/actuator/health"))
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void createUserTestSuccess() throws Exception {
        //given
        final ObjectMapper objectMapper = new ObjectMapper();
        final CreateUserDto createUserDto = CreateUserDto.builder()
            .email("email@email.com")
            .phone("phone")
            .nickname("nickname")
            .build();
        //when create
        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createUserDto)))
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andReturn();
        //then
        final String id = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), String.class);
        Assertions.assertNotNull(id);

        // when get
        mockMvc.perform(MockMvcRequestBuilders.get("/users/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.equalTo(id)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.equalTo(createUserDto.getEmail())))
            .andExpect(MockMvcResultMatchers.jsonPath("$.phone", Matchers.equalTo(createUserDto.getPhone())))
            .andExpect(MockMvcResultMatchers.jsonPath("$.nickname", Matchers.equalTo(createUserDto.getNickname())))
        ;

        createUserDto.setPhone("new phone");
        // when put
        mockMvc.perform(MockMvcRequestBuilders.put("/users/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createUserDto)))
            .andExpect(MockMvcResultMatchers.status().isOk());

        // when get
        mockMvc.perform(MockMvcRequestBuilders.get("/users/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.equalTo(id)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.email", Matchers.equalTo(createUserDto.getEmail())))
            .andExpect(MockMvcResultMatchers.jsonPath("$.phone", Matchers.equalTo(createUserDto.getPhone())))
            .andExpect(MockMvcResultMatchers.jsonPath("$.nickname", Matchers.equalTo(createUserDto.getNickname())));

        // when delete
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk());
        mockMvc.perform(MockMvcRequestBuilders.get("/users/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isNotFound());

    }

}