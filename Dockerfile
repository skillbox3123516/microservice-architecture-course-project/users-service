FROM adoptopenjdk:11-jre-openj9
COPY /build/libs/*.jar app.jar
ENTRYPOINT ["sh", "-c", "java -jar app.jar"]
EXPOSE 8080